package com.cource_api.topicController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TopicService {

	private List<Topic> topics = new ArrayList<>(Arrays.asList(new Topic("Java", "Java 1.8", "Java 1.8 Description"),
			new Topic("Python", "Python2.0", "Pyhton Description"),
			new Topic("Spring", "Spring Freamwork", "Spring Freamwork Description")));

	public List<Topic> getAllTopics() {
		return topics;
	}

	public Topic getTopic(String Id) {
		return topics.stream().filter(t -> t.getId().equals(Id)).findFirst().get();
	}

	public void addTopic(Topic topic) {
		topics.add(topic);

	}

	public void updateTopic(String id, Topic topic) {
		for (int i = 0; i <= topics.size(); i++) {
			Topic t = topics.get(i);
			if (t.getId().equals(id)) {
				topics.set(i, topic);
				return;
			}
		}

	}

	public void deleteTopic(String id) {
		topics.removeIf(t -> t.getId().equals(id));
	}

}
